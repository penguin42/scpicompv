use clap::Parser;
use flexbuffers;
use itertools::Itertools;
use log::*;
use serde::{Deserialize, Serialize};
use std::cmp::{min,max};
use std::fs::File;
use std::io::{BufRead,BufReader,Read,Write,Error,ErrorKind};
use std::net::TcpStream;
use std::path::PathBuf;
use std::str;
use std::thread;
use std::time;

use image::{GrayImage,Luma};

#[derive(clap::ValueEnum, Clone, PartialEq)]
enum TriggerMode {
    None,
    Beeb,
    Pulseneg, // Found to be useful on an Apple2c
}

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Args {
    /// Use a previously saved trace instead of capturing from scope
    #[arg(short, long, value_name = "FILE")]
    read_trace: Option<PathBuf>,

    /// Write received trace to a file
    #[arg(short, long, value_name = "FILE")]
    write_trace: Option<PathBuf>,

    /// Set trigger mode
    #[arg(short, long, value_enum, default_value = "beeb")]
    trigger_mode: TriggerMode,
}

const CHANNEL: usize = 1;
const NCHANNELS: usize = 2;

const PAL_FIELDLINES: usize = 312;
const PAL_LINETIME: f64 = 0.000064;
const IMAGE_WIDTH: usize = 1024;

#[derive(Default, Debug,PartialEq,Clone,Copy,Serialize, Deserialize)]
struct WaveParams {
    points: usize,
    xinc: f64,  // delta between points in seconds
    xorigin: f64, // the time of the first point
    yinc: f64,
    yorigin: f64,
    yref: f64,
}

struct SCPIStream<T: Read + Write> {
    br: BufReader<T>,
}

impl<T: Read + Write> SCPIStream<T> {
    fn new(s: T) -> SCPIStream<T> {
        SCPIStream { br: BufReader::new(s) }
    }

    fn send(&mut self, cmd: &str) -> std::io::Result<()> {
        debug!("send: '{cmd}'");

        let termcmd = cmd.to_string() + "\n";
        self.br.get_mut().write_all(termcmd.as_bytes())
    }

    fn query(&mut self, cmd: &str) -> std::io::Result<String> {
        self.send(cmd)?;

        let mut line = String::new();
        self.br.read_line(&mut line)?;
        line = line.trim_end_matches('\n').to_string();

        debug!("query: '{line}'");
        Ok(line)
    }

    fn fetch_trigger_status(&mut self) -> std::io::Result<String> {
        self.query(":TRIG:STAT?")
    }

    fn wait_trigger_status(&mut self, desired: &str) -> std::io::Result<()> {
        debug!("Waiting for trigger status '{desired}'");

        while self.fetch_trigger_status()? != desired {
            thread::sleep(time::Duration::from_millis(100));
        }
        debug!("At trigger '{desired}'");
        Ok(())
    }

    fn fetch_wave_params(&mut self) -> std::io::Result<WaveParams> {
        let wp_string = self.query(":WAV:PRE?")?;

        // We're expecting something like 0,0,1200,1,1.000000e-06,-3.000000e-04,0,8.000000e-02,-149,127
        let wp_vec: Vec<&str> = wp_string.split(',').collect();
        debug!("Wave params: {:#?}", wp_vec);

        if wp_vec.len() != 10 {
            return Err(Error::new(ErrorKind::Other, format!("Bad wave param vec: '{}'", wp_string)));
        };

        /* 0: Format */
        /* 1: Type */
        /* 2: Point count */
        let points: usize = wp_vec[2].parse::<usize>()
                                     .map_err(|e| Error::new(ErrorKind::Other,
                                                             format!("Failed to parse Wave points: '{}'; {}", wp_vec[2], e)))?;
        /* 3: count: The number of averages */
        /* 4: xincrement */
        let xinc: f64 = wp_vec[4].parse::<f64>()
                                 .map_err(|e| Error::new(ErrorKind::Other, format!("Failed to parse Wave xinc: '{}'; {}", wp_vec[4], e)))?;
        /* 5: xorigin */
        let xorigin: f64 = wp_vec[5].parse::<f64>()
                                 .map_err(|e| Error::new(ErrorKind::Other, format!("Failed to parse Wave xorigin: '{}'; {}", wp_vec[5], e)))?;
        /* 6: xreference, always 0 */
        /* 7: yincrement */
        let yinc: f64 = wp_vec[7].parse::<f64>()
                                 .map_err(|e| Error::new(ErrorKind::Other, format!("Failed to parse Wave yinc: '{}'; {}", wp_vec[7], e)))?;
        /* 8: yorigin */
        let yorigin: f64 = wp_vec[8].parse::<f64>()
                                 .map_err(|e| Error::new(ErrorKind::Other, format!("Failed to parse Wave yorigin: '{}'; {}", wp_vec[8], e)))?;
        /* 9: yreference */
        let yref: f64 = wp_vec[8].parse::<f64>()
                                 .map_err(|e| Error::new(ErrorKind::Other, format!("Failed to parse Wave yref: '{}'; {}", wp_vec[9], e)))?;

        Ok(WaveParams { points: points, xinc: xinc, xorigin: xorigin, yinc: yinc, yorigin: yorigin, yref: yref })
    }

    // TODO: Think about interface, a bit weird to pass params in
    // we also probably don't need the whole buffer?
    fn fetch_wave(&mut self, data: &mut Vec<u8>, cur_params: WaveParams) -> std::io::Result<()> {
        // The Rigol DS1000Z-E manual says max is 250k, 1M seems to work for me and is a lot faster
        // 4M fails returning 0 bytes
        const MAXDATAPERIT: usize = 250000*4;
        let mut cur : usize = 0;

        data.resize(cur_params.points, 0);

        while cur < cur_params.points {
            let this_count = min(cur_params.points-cur, MAXDATAPERIT);
            debug!("Fetching wave data from {}", cur);

            // Note the scope indexes from 1
            self.send(&format!(":WAV:STAR {}", cur+1))?;
            self.send(&format!(":WAV:STOP {}", cur+this_count))?;

            // Data please
            self.send(":WAV:DATA?")?;

            // Read header of form #9????????? where that's bytes of data
            let mut header = [0 as u8; 11];
            self.br.read_exact(&mut header)?;
            debug!("Wave data header: {:?}", header);
            if header[0] != b'#' || header[1] != b'9' {
                return Err(Error::new(ErrorKind::Other, format!("Bad wave data header: {:?}", header)));
            }
            let bytecount_text = str::from_utf8(&header[ 2 .. 11]).
                                      map_err(|_| Error::new(ErrorKind::Other, format!("Failed to utf8 wave data header count")))?;
            let bytecount = bytecount_text.parse::<usize>().
                                      map_err(|_| Error::new(ErrorKind::Other, format!("Failed to parse wave data header count '{}'", bytecount_text)))?;

            if bytecount == 0 {
                return Err(Error::new(ErrorKind::Other, format!("Wave data, scope gave us no data at {cur}")));
            }
            if bytecount > this_count {
                return Err(Error::new(ErrorKind::Other, format!("Wave data, scope gave us more data at {cur}; {bytecount} vs {this_count}")));
            }

            // Read data
            self.br.read_exact(&mut data[cur..(cur+bytecount)])?;

            // There's a '\n' at the end, lets eat that.
            self.br.read_exact(&mut header[0..1])?;
            if header[0] != b'\n' {
                return Err(Error::new(ErrorKind::Other, format!("wave data expecting a \n got {}", header[0])));
            }
            cur+=bytecount;
        }
        Ok(())
    }
}

// Find a falling edge of at least 'delta_thresh' height and where the value after the edge
// is smaller than floor_thresh.  The result is Some(index) where index is the offset into
// the input slice of the edge.
fn find_falling_sync(data: &[u8], delta_thresh: f64, floor_thresh: f64) -> Option<usize> {
    let decimation = 10; // So we get a nice sharp edge
    let edged = data.iter()
        .step_by(decimation)
        .tuple_windows::<(_,_,_)>()
        // Simple sobel like edge detect, -2.0,0,2.0 , gives a tuple with that and
        // the 3rd value
        .map(|w| (((*w.0 as f64 * -2.0)+(*w.2 as f64 * 2.0),
                   *w.2)))
        .position(|e| (e.0 < delta_thresh) && ((e.1 as f64) < floor_thresh))
        // If we found an edge, the value is decimated and is the index to the first point, fix that up
        .and_then(|i| Some((i+1)*decimation));
    println!("Edge raw: {:?}", edged);
    edged
}

fn main() -> std::io::Result<()> {
    env_logger::init();

    let args = Args::parse();
    let mut cur_params : WaveParams;
    let mut wav: Vec<u8> = Vec::new();

    if let Some(read_trace_path) = args.read_trace.as_deref() {
        // Read a trace from file rather than the scope
        let mut file = match File::open(&read_trace_path) {
            Err(why) => panic!("Couldn't open read-trace file {}: {}", read_trace_path.display(), why),
            Ok(file) => file,
        };
        let mut file_data: Vec<u8> = Vec::new();
        file.read_to_end(&mut file_data)?;

        let reader = flexbuffers::Reader::get_root(file_data.as_slice()).unwrap();

        (cur_params,wav) = <(WaveParams,Vec<u8>)>::deserialize(reader).unwrap();
        println!("Read vector length ={}", wav.len());
        println!("Read Params: {:#?}", cur_params);
        //println!("Vector! {:#?}", wav);
    } else {
        let netcon = TcpStream::connect("ronnie:5555")?;
        let mut ss = SCPIStream::new(netcon);
    
        let idn = ss.query("*IDN?\n")?;
        println!("Device says '{idn}'");
    
        // Just enable the channel we want
        ss.send(&format!(":CHAN{}:DISP ON", CHANNEL))?;
    
        // TODO: Get no of channels by :SYS:RAM
        for c in 1..=NCHANNELS {
            if c != CHANNEL {
                ss.send(&format!(":CHAN{}:DISP OFF", c))?;
            }
        }
    
        // Set up timebase, we want one frame (20ms) across
        // We have 12 divs across, so 20/12=1.66ms, so go for
        // 2 ms
        ss.send(":TIM:MODE MAIN")?;
        ss.send(":TIM:MAIN:SCALE 0.002")?;
        ss.send(":TIM:DEL:ENAB OFF")?;
    
        // Set up memory depth to get us enough bandwidth
        // Depressingly, this is all tied to the timebase rather
        // than sample rate, so we start with our timebase
        // above (2ms), we've got 12 divs, so that's 24ms
        // and we've only got a few options to chose from
        // on the Rigol.  We don't want to use more than
        // necessary otherwise it'll slow the download
        // So 12MS depth gets us I think 50MS/second, or 25MHz
        // bandwidth, which is way more than we need
        // Note: For no apparent reason we must be running
        // to set the depth
        // ....which is all the theory but just doesn't happen
        // at 12Msam depth, 2MS/div I'm getting 250MSam/s
        // and even with DEL off, I'm getting the trigger at -2.4ms
        ss.send(":RUN")?;
        ss.send(":ACQ:MDEP 12000000")?;
    
        // Set up trigger
        match args.trigger_mode {
            TriggerMode::Beeb => {
                ss.send(":TRIG:MODE VID")?;
                ss.send(&format!(":TRIG:VID:SOUR CHAN{}", CHANNEL))?;
                ss.send(":TRIG:VID:STAN PALS")?;
                ss.send(":TRIG:VID:MODE ODDF")?;
            },

            TriggerMode::Pulseneg => {
                // A negative pulse of at least 10us finds us vsync
                ss.send(":TRIG:MODE PULS")?;
                ss.send(":TRIG:PULS:WHEN NGR")?; // Negative going, width greater than
                ss.send(&format!(":TRIG:PULS:SOUR CHAN{}", CHANNEL))?;
                ss.send(":TRIG:PULS:WIDT 1.000000e-05")?;
                // I don't think we need the UWID or LWID in 'greater than' mode?
                // You do need to get the level set
            },

            TriggerMode::None => (),
        }
    
        ss.send(":STOP")?;
    
        ss.wait_trigger_status("STOP")?;
    
        // OK, main grab frame/field
        ss.send(":SING")?;
        ss.wait_trigger_status("STOP")?;
    
        ss.send(":WAV:MODE RAW")?;
        // My Rigol is 8 bit only, 16bit mode always 0's top byte apparently
        ss.send(":WAV:FORM BYTE")?;
    
        cur_params = ss.fetch_wave_params()?;
    
        // I've had problems in other programs with the trigger taking a while
        // to start before we even notice it stopped, so just check the parameters
        // settled
    
        loop {
            thread::sleep(time::Duration::from_millis(20));
            let new_params = ss.fetch_wave_params()?;
            if new_params == cur_params {
                break;
            }
    
            // Mismatch, go around again
            cur_params = new_params;
            ss.wait_trigger_status("STOP")?;
        };
    
        ss.fetch_wave(&mut wav, cur_params)?;
        println!("Params: {:#?}", cur_params);
    }
    
    if let Some(write_trace_path) = args.write_trace.as_deref() {
        // Save the gathered trace to file
        let mut file = match File::create(&write_trace_path) {
            Err(why) => panic!("Couldn't create write-trace file {}: {}", write_trace_path.display(), why),
            Ok(file) => file,
        };
        // Write the parameters and data
        let mut ser = flexbuffers::FlexbufferSerializer::new();
        (cur_params,wav.clone()).serialize(&mut ser).unwrap();
        file.write_all(ser.view()).unwrap();
    }

    // OK, time to create an image; I could just add a header and dump the
    // bytes, but I want to scale for the contrast and I'll eventually
    // want to crop off the pulses
    let mut img = GrayImage::new(IMAGE_WIDTH as u32, PAL_FIELDLINES as u32);

    // Hopefully the trigger has put us on the edge of a sync
    let first_pixel_index = (-cur_params.xorigin / cur_params.xinc) as usize;

    // Try and determine the black and sync levels; they should be either
    // side of the sync, but there might be other stuff like colour bursts
    // and depending on the sync I'm not betting on which side is which.
    // in the following: lower being before the sync, upper after
    let search_range = 50; // in samples
    let lower_index = max(first_pixel_index-search_range, 0);
    let lower_data = &wav[lower_index..first_pixel_index];
    let lower_sum = lower_data.iter().fold(0.0, |acc, d| acc+(*d as f64));
    let lower_mean = (lower_sum as f64) / (lower_data.len() as f64);

    let upper_index = min(first_pixel_index+search_range, wav.len());
    let upper_data = &wav[first_pixel_index..upper_index];
    let upper_sum = upper_data.iter().fold(0.0, |acc, d| acc+(*d as f64));
    let upper_mean = (upper_sum as f64) / (upper_data.len() as f64);

    // Whichever is higher is the black level, the lower is the sync level
    let black_level = lower_mean.max(upper_mean);
    let sync_level = lower_mean.min(upper_mean);
    // But as a threshold, halfway between the two - anything lower is sync
    let sync_threshold = (black_level+sync_level) / 2.0;
    // How big an edge isn't noise? How about half the difference of levels?
    let edge_threshold = (black_level - sync_level) / 2.0;

    println!("sync ranges: lower:{} upper: {} sync thresh: {} edge thresh: {}", lower_mean, upper_mean, sync_threshold, edge_threshold);

    // Theory is that the black-sync is supposed to be 0.3v, and the content is 0.7v
    let vert_scale = (black_level - sync_level) * 0.7 / 0.3;

    let mut next_line_start_index = first_pixel_index;
    let line_samples = (PAL_LINETIME / cur_params.xinc) as usize;

    for l in 0..PAL_FIELDLINES {
        // Time for this line, then take off the offset from 0
        let line_start_index = next_line_start_index;

        if line_start_index > wav.len() {
            break;
        }
        debug!("Line {l} start_index={line_start_index}");

        // Search forward for the next edge for the next line, note that we start halfway along
        // the current line to make sure we've really missed our current edge and also anything
        // like colour bursts
        let edge = if (line_start_index+line_samples/2) < wav.len() {
            find_falling_sync(&wav[line_start_index+line_samples/2..], -edge_threshold, sync_threshold)
        } else {
            None
        };
        match edge {
          None =>
            // if we don't know where the next line is, make it slightly fast so we then find the
            // edge
            next_line_start_index = line_start_index + ((PAL_LINETIME - 0.000001) / cur_params.xinc) as usize,
          Some(e) =>
            next_line_start_index = line_start_index + line_samples/2 + e,
        }

        for pdst in 0..IMAGE_WIDTH {
            let pixel_time = pdst as f64* PAL_LINETIME / IMAGE_WIDTH as f64;
            let pixel_index = (pixel_time / cur_params.xinc) as usize + line_start_index as usize;
            if pixel_index > wav.len() {
                break;
            }
            //debug!("pixel {p} pixel_time={pixel_time} pixel_index={pixel_index}");

            let mut tmp_pixel = wav[pixel_index] as f64 - black_level;
            tmp_pixel=tmp_pixel/vert_scale;
            tmp_pixel*=255.0;
            tmp_pixel=tmp_pixel.min(255.0).max(0.0);
            let pixel = [tmp_pixel as u8];

            img.put_pixel(pdst as u32, l as u32, Luma::<u8>::from(pixel));
        }
    }

    img.save("field.png").map_err(|e| Error::new(ErrorKind::Other, format!("Image save failed: '{}'", e.to_string())))?;
    Ok(())
}
