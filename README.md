## Intro

This is a hack to extract a PAL composite video field from a scope over SCPI via TCP/IP.

## Tested setup

This is just a hack, it's had very little testing.

### Scope

This has been tested with a Rigol DS1102Z-E running firmware 00.06.02.

It'll almost certainly need modifying for any other scope, SCPI is about as
portable as a boulder.

### Video signal

I've tried two setups:

- An Apple 2c composite output, both in text mode and graphics mode.  See the apple2-0.1 tag.

- A BBC Model B's composite output (without any colour burst so far)
and have currently not got a terminator in circuit.  That was tested on the tag beeb0.1 but
hasn't been retested since.

### Host

This was written and tested on Fedora 41 Linux on a PC.

## Usage

At the moment the hostname of my scope is hardcoded, I guess I should parameterise it!

Wire the input to channel 1, set the offsets and amplitude so that the signal fills the
display.  Set the trigger level in about the bottom quarter of the signal to hit the
sync pulses; about 250mv works nicely for the Apple2c setup.

Running the program sets up the trigger mode and timebase etc and captures
a field.  The *-t* option can be *none* (which uses your current trigger settings),
*beeb* (which uses video syncing in PAL mode) and *pulseneg* which uses a
negative going pulse of at least 10us to find the vsync.  'pulseneg' has
been tested on the Apple2c, and 'beeb' has been tested on the Model B.

If it doesn't behave run with **RUST_LOG=debug** for loads of debug.

## Notes

### Horizontal and timebase

My Rigol allows me to set an aquisition depth but not sample rate or length.
The length come from the horizontal timebase settings and the scope
then uses that to work backwards from the depth to pick a sample rate.
It then seems to add a before/after as well.  I just picked settings that give
me enough bandwidth, but I could probably play to get smaller captures.

### Vertical and data

This Rigol is 8 bit and I use the byte waveform mode to dump the data.  I think
this pretty much corresponds to 256 points vertically on the display.

The Rigol refuses to give all the data in one command, so I need to use multiple
commands.   The amount you can request in a single command is ill defined - I seem
to be able to fetch larger chunks than the DS1000Z manual says.  Bigger chunks
is faster, which given the slow speed is useful.

I then use this raw data to build the image with not much processing.

## TODO

* Parameterise IP address
* Break out SCPI code into a separate file
* Support USB ?
* Grab only the part of the waveform we need
* Both fields
* Colour?

## Author

David Alan Gilbert, (mailto:dave@treblig.org)
